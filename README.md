# Quantum GIS support material for Water and Sanitation Authorities (WSSA) in Tanzania

## Administration and end-user documentation

Have a look at the [Wikipedia documentation](https://gitlab.com/wssa-gis-support-tz/qgis-support/-/wikis/QQGIS-reference-material-for-Water-and-Sanitaion-Authorities) for reference and training material.